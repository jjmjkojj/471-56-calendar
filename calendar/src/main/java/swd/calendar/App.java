package swd.calendar;

import java.io.IOException;
import java.text.ParseException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
    	Account ac = new Account();
    	CalendarManager cm = new CalendarManager(ac);
        CalendarGUI cGUI = new CalendarGUI(cm); 
        
        CalendarConsole cc = new CalendarConsole(cm);
        FrameSortByDesc fSortByDesc = new FrameSortByDesc(cm);
        FrameSortByDate fSortByDate = new FrameSortByDate(cm);
        FrameSortByDuration fSortByDuration = new FrameSortByDuration(cm);
        
        ac.addObserver(fSortByDesc);
        ac.addObserver(fSortByDate);
        ac.addObserver(fSortByDuration);
    }
}
