package swd.calendar;

import java.awt.GridLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class FrameSortByDate extends JFrame implements Observer {
	
	private CalendarManager cm;

	private Border etched = BorderFactory
			.createEtchedBorder(EtchedBorder.LOWERED);
	private TitledBorder resultBordor;
	private JPanel resultArea;
	private JScrollPane scrollPane;
	private JTextArea resultTextArea;
	
	private String showResult;
	
	private static final int FRAME_WIDTH = 512;
	private static final int FRAME_HEIGHT = 360;
	
	public FrameSortByDate(CalendarManager cm) {
		this.cm = cm;
		instantiateJComponents();
		setLayouts();
		setDefault();
	}
	
	private void instantiateJComponents() {
		setTitle("Atikom Calendar ^-^ : Display By Date");
		
		resultArea = new JPanel();
		resultBordor = BorderFactory.createTitledBorder(etched, "Result");
		resultArea.setBorder(resultBordor);
		
		resultTextArea = new JTextArea();
		resultTextArea.setEditable(false);
		
		scrollPane = new JScrollPane(resultTextArea);
	}
	
	private void setLayouts() {
		setSize(FRAME_WIDTH, FRAME_HEIGHT);
		setLocation(175, 475);
		setVisible(true);	
		
		add(resultArea);
		
		resultArea.setLayout(new GridLayout());
		
		resultArea.add(scrollPane);
	}
	
	private void setDefault() {
		showResult = cm.display(cm.getTimeDurByDate(), cm.getTotalTime());
		resultTextArea.setText(showResult);
	}
	
	public void update(Observable o, Object arg) {
		showResult = cm.display(cm.getTimeDurByDate(), cm.getTotalTime());
		resultTextArea.setText(showResult);
	}
}
