package swd.calendar;

import java.awt.List;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class CalendarView {
	
	private String welcomeMsg = "Welcome to Atikom Calendar ^-^";
	private String doneMsg = "Done adding!";
	private final String newLine = "\n";
	
	private DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
	
	public String getWelcomeMsg(){
		return welcomeMsg;
	}
	
	public String getDoneMsg(){
		return doneMsg;
	}
	
	public String getResult(ArrayList<TimeDuration> t, int totalTime){
		String tmp = "Items\t\tType\tEst.Time\tDate\tDone\n"
				+ "=====\t\t====\t========\t====\t====\n";
		for (int i = 0 ; i<t.size(); i++){
			String type = t.get(i).getClass().getSimpleName();
//			System.out.println(type);
			String done;
			if (type.endsWith("Event")) {
				done = "--";
			} else {
				done = "NO";
			}
			tmp = tmp + (t.get(i).getDescription() + "\t\t" + type
					+ "\t" + t.get(i).getDuration() + "\t"
					+ dateFormat.format(t.get(i).getDate()) + "\t" + done
					+ newLine);
		}
		tmp = tmp + newLine+"Total estimated time : "+totalTime+" minutes"+newLine+newLine;
		return tmp;
	}
}
