package swd.calendar;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

/**
 * show Calendar event and task
 * 
 * @author JJJ
 * 
 */
public class CalendarGUI extends JFrame{
	private CalendarManager cm;

	private Border etched = BorderFactory
			.createEtchedBorder(EtchedBorder.LOWERED);
	private TitledBorder displayBorder, createBordor, resultBordor;

	private JPanel mainArea, userArea, createArea, displayArea, resultArea,
			inputPanel;
	private JLabel typeLabel, descriptionLable, fromLable, toLable,displayLable;
	private JScrollPane scrollPane;
	private JTextField descriptionField, fromField, toField;
	private JTextArea resultTextArea;
	private JButton sortByDateButton, sortByDescrptionButton,
			sortByDurationButton, addButton;
	private JComboBox typeComboBox;

	private static final int FRAME_WIDTH = 512;
	private static final int FRAME_HEIGHT = 360;
	private int typeSelected = 0;
	private final String fromDefault = "From  :  ";
	private final String toDefault = "To  :  ";
	private final String dueDateDefault = "Due Date  :  ";
	private final String estTimeDefault = "Estimated Duration  :  ";
	private final String[] type = { "Event", "Task" };
	
	private String showResult = "";

	/**
	 * Construct a frame for use calendar
	 * 
	 * @throws Exception
	 */
	public CalendarGUI(CalendarManager cm2) throws Exception {
		cm = cm2;
		readFile();
		instantiateJComponents();
		setLayouts();
		setBordor();
		addListeners();
		descriptionField.requestFocus();
	}

	/**
	 * Read data from "calendar.txt"
	 * 
	 * @throws Exception
	 */
	private void readFile() throws Exception {
		cm.loadData();
	}

	/**
	 * Instantiate all necessary JComponent objects.
	 */
	private void instantiateJComponents() {
		setTitle("Atikom Calendar ^-^ : Create Event and Task");

		sortByDateButton = new JButton("Sort by date");
		sortByDescrptionButton = new JButton("Sort by descrption");
		sortByDurationButton = new JButton("Sort by duration");
		// displayButton.set
		
		// resultTextArea.setFont(new Font("Tahoma", 0, 16));
		resultTextArea = new JTextArea(cm.getWelcomeMsg());
		scrollPane = new JScrollPane(resultTextArea);
		resultTextArea.setEditable(false);


		typeLabel = new JLabel("Type  :  ");
		typeLabel.setHorizontalAlignment(JLabel.RIGHT);
		descriptionLable = new JLabel("Description  :  ");
		descriptionLable.setHorizontalAlignment(JLabel.RIGHT);
		fromLable = new JLabel(fromDefault);
		fromLable.setHorizontalAlignment(JLabel.RIGHT);
		toLable = new JLabel(toDefault);
		toLable.setHorizontalAlignment(JLabel.RIGHT);

		typeComboBox = new JComboBox(type);
		descriptionField = new JTextField(20);
		fromField = new JTextField(20);
		toField = new JTextField(20);

		addButton = new JButton("Add");
		
		displayLable = new JLabel(" ");
	}

	/**
	 * Set bordor for create area , display area and result area
	 */
	public void setBordor() {
		createBordor = BorderFactory.createTitledBorder(etched, "Create");
		createArea.setBorder(createBordor);

		displayBorder = BorderFactory.createTitledBorder(etched, "Display");
		displayArea.setBorder(displayBorder);

		resultBordor = BorderFactory.createTitledBorder(etched, "Result");
		resultArea.setBorder(resultBordor);
	}

	/**
	 * Set simple layout
	 */
	private void setLayouts() {
		mainArea = new JPanel();
		mainArea.setLayout(new GridLayout(1, 2));

		userArea = new JPanel();
		userArea.setLayout(new BorderLayout());

		displayArea = new JPanel();
		displayArea.setLayout(new FlowLayout());
//		displayArea.add(sortByDateButton);
//		displayArea.add(sortByDescrptionButton);
//		displayArea.add(sortByDurationButton);
		displayArea.add(displayLable);

		createArea = new JPanel();
		createArea.setLayout(new BorderLayout());

		inputPanel = new JPanel();
		inputPanel.setLayout(new GridLayout(4, 2));
		inputPanel.add(typeLabel);
		inputPanel.add(typeComboBox);
		inputPanel.add(descriptionLable);
		inputPanel.add(descriptionField);
		inputPanel.add(fromLable);
		inputPanel.add(fromField);
		inputPanel.add(toLable);
		inputPanel.add(toField);

		JPanel buttonPanel = new JPanel();
		buttonPanel.setLayout(new FlowLayout());
		buttonPanel.add(addButton);

		createArea.add(inputPanel, BorderLayout.CENTER);
		createArea.add(buttonPanel, BorderLayout.SOUTH);

		resultArea = new JPanel();
		resultArea.setLayout(new BorderLayout());
		resultArea.add(scrollPane, BorderLayout.CENTER);

		mainArea.add(userArea);
//		mainArea.add(resultArea);

		userArea.add(createArea);
		userArea.add(displayArea, BorderLayout.SOUTH);

		getContentPane().setLayout(new GridLayout(1, 2));
		getContentPane().add(mainArea);

		setSize(FRAME_WIDTH, FRAME_HEIGHT);
//		setExtendedState(JFrame.MAXIMIZED_BOTH);
		setLocation(700,100);
		setResizable(false);
		setVisible(true);
	}

	/**
	 * Add listeners for all buttons
	 */
	private void addListeners() {
		addCloseWindowListener();
		addButtonAddListener();
		addTypeComboBoxListener();
		addDisplayListener();
	}

	/**
	 * Add listener to close GUI window and write data to "calendar.txt" when
	 * user click the exit button on frame
	 */
	private void addCloseWindowListener() {
		// when click on the close button, it quits
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				try {
					cm.saveData();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
	}

	/**
	 * Add action when combo box state change
	 */
	private void addTypeComboBoxListener() {
		typeComboBox.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				// TODO Auto-generated method stub
				typeSelected = typeComboBox.getSelectedIndex();
				if (typeSelected == 0) {
					fromLable.setText(fromDefault);
					toLable.setText(toDefault);
				} else {
					fromLable.setText(dueDateDefault);
					toLable.setText(estTimeDefault);
				}

			}
		});
	}

	/**
	 * Add action to the add button
	 */
	private void addButtonAddListener() {
		addButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				String desc = descriptionField.getText().trim();
				String from = fromField.getText().trim();
				String to = toField.getText().trim();

				if (desc.isEmpty() || from.isEmpty() || to.isEmpty()) {
					JOptionPane.showMessageDialog(userArea,
							"โปรดใส่ข้อมูลให้ครบทุกช่อง");
					if (desc.isEmpty()) {
						descriptionField.requestFocus();
					} else if (from.isEmpty()) {
						fromField.requestFocus();
					} else {
						toField.requestFocus();
					}
				} else {
					String type = (String) typeComboBox.getItemAt(typeSelected);
					try {
						cm.create(type, desc, from, to);
//						resultTextArea.setText(cm.getDoneMsg());
						displayLable.setText(cm.getDoneMsg());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				descriptionField.setText(null);
				fromField.setText(null);
				toField.setText(null);
			}
		});
	}

	/**
	 * Add action to the Sort by date button
	 */
	private void addDisplayListener() {
		sortByDateButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
//				resultTextArea.setText(defaultDisplay);
//				for (TimeDuration t : ac.list) {
//					String type = t.getClass().getSimpleName();
//					String done;
//					if (type.endsWith("Event")) {
//						done = "--";
//					} else {
//						done = "NO";
//					}
//					resultTextArea.append(t.getDescription() + "\t\t" + type
//							+ "\t" + t.getDuration() + "\t"
//							+ dateFormat.format(t.getDate()) + "\t" + done
//							+ newLine);
//				}
//				resultTextArea.append("\nTotal estimated time:  "
//						+ ac.getTotalEstimatedTime() + "  minutes");
				showResult = cm.display(cm.getTimeDurByDate(), cm.getTotalTime());
				resultTextArea.setText(showResult);
//				showResult = cm.display(tmp, cm.getTotalTime());
			}
		});
		sortByDescrptionButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				// TODO Auto-generated method stub
				showResult = cm.display(cm.getTimeDurByDesc(), cm.getTotalTime());
				resultTextArea.setText(showResult);
			}
		});
		sortByDurationButton.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				showResult = cm.display(cm.getTimeDurByDuration(), cm.getTotalTime());
				resultTextArea.setText(showResult);
			}
		});
	}

}
