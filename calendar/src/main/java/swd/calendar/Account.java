package swd.calendar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;

public class Account extends Observable {
	protected String name = "Atikom Calendar ^-^";
	protected ArrayList<Event> events = new ArrayList<Event>();
	protected ArrayList<Task> tasks = new ArrayList<Task>();
	protected ArrayList<TimeDuration> list = new ArrayList<TimeDuration>();
	protected ArrayList<TimeDuration> listForTest = new ArrayList<TimeDuration>();
	/**
	 * 
	 * @param t TimeDuration ที่รับเข้ามา
	 */
	public void add (TimeDuration t){
//		int i = 0;
//		if (list.size() == 0){
//			list.add(i,t);
//		} else {
//			for( ; i<list.size();i++){
//				if( t.getDate().getTime() < list.get(i).getDate().getTime()){
//					break;
//				}
//			}
//			list.add(i,t);
//		}
		list.add(t);
		setChanged();
		notifyObservers(list);
//		Collections.sort(list);
	}
	/**
	 * 
	 * @return ผลรวมของ EstimatedTime
	 */
	public int getTotalEstimatedTime(){
		int total = 0;
		for(int i = 0 ; i < list.size();i++){
			total += list.get(i).getDuration(); 
		}
		return total;
	}
	/**
	 * ใช้สำหรับ test
	 * @param i index ที่จะนำไปเช็ค
	 * @return description ของ index ที่รับมา
	 */
	public String testSort(int i){
		return list.get(i).getDescription();
	}
	
	
}
