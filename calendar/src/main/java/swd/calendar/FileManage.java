package swd.calendar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class FileManage {
	private Account ac;
	private Event event;
	private Task task;
	private DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");

	/**
	 * 
	 * @param ac รับ Account มาเพื่อให้ เพิ่มไปใน Account ตัวเดียวกัน
	 */
	public FileManage(Account ac) {
		this.ac = ac;
	}

	/**
	 * อ่านข้อมูลจาก calendar.txt แล้วเพิ่มไปใน list 
	 * @throws Exception
	 */
	public void readFile() throws Exception {
		BufferedReader br = new BufferedReader(new FileReader("calendar.txt"));
		String data;
		while ((data = br.readLine()) != null) {
			data.trim();
			String[] info = data.split(",");
			if (info[0].equals("Event")) {
				event = new Event(info[1].trim(), dateTimeFormat.parse(info[2]
						.trim()), dateTimeFormat.parse(info[3].trim()));
				ac.add(event);
			} else {
				task = new Task(info[1].trim(),
						dateFormat.parse(info[2].trim()),
						Integer.parseInt(info[3].trim()));
				ac.add(task);
			}
		}
	}

	/**
	 * เขียนข้อมูลใน list ลงใน calendar.txt
	 * @throws Exception
	 */
	public void writeFile() throws Exception {
		String line = "";
		BufferedWriter writer = new BufferedWriter(new FileWriter(
				"calendar.txt"));
		for (TimeDuration a : ac.list) {
			String type = a.getClass().getSimpleName();
			if ( type.equals("Event")){
				line = type + ","
						+ a.getDescription() + "," + dateTimeFormat.format(a.getDate()) + ","
						+ dateTimeFormat.format(a.getDuration()*60000+a.getDate().getTime())+"\n";
			}
			else {
				line = type + ","
						+ a.getDescription() + "," + dateFormat.format(a.getDate()) + ","
						+ a.getDuration()+"\n";
			}
			writer.write(line);
		}
		writer.flush();
		writer.close();
		System.exit(0);
	}
}
