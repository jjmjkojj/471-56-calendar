package swd.calendar;

import java.util.Date;

public interface TimeDuration extends Comparable<TimeDuration> {

	public String getDescription();
	public Date getDate();
	public int getDuration();
}
