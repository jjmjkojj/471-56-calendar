package swd.calendar;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Event extends AbstractTimeDuration {
	//	private String desciption;
	private Date from;
	private Date to;
	private int estTime;
	/**
	 * 
	 * @param des รับชื่อ งาน
	 * @param from วันเวลาที่เริ่มทำงาน
	 * @param to วันเวลาที่สิ้นสุด
	 */
	public Event(String desc,Date from, Date to){
		setDescriotion(desc);
		this.to = to;
		this.from = from;
	}
	
	/**
	 * return ระยะเวลาจาก วันที่เริ่ม ถึง วันที่สิ้นสุด เป็น นาที
	 */
	public int getDuration(){
		estTime = (int)(to.getTime() - from.getTime()) / (60*1000);
		return estTime;
	}
	/**
	 * return วันที่เริ่ม
	 */
	public Date getDate(){
		return from;
	}
	/**
	 * return ชื่องาน
	 */
//	public String getDescription(){
//		return desciption;
//	}
	
//	public int compareTo(TimeDuration o) {
//		return this.getDate().compareTo(o.getDate());
//	}
	
}
