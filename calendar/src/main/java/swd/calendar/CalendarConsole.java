package swd.calendar;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.ParseException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

import javax.swing.text.html.parser.Parser;

public class CalendarConsole {
	private CalendarManager cm;
	private String desc;
	private String type;
	private int estTime;
	private Date date;
	private DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
public CalendarConsole(CalendarManager cm2) {
		cm = cm2;
	}
/**
 * 
 * ใช้ตอบโต้กับ ผู้ใช้ ในการใช้โปแกรม
 * c = สร้าง event หรือ task
 * d = โชว์ event หรือ task ทั้งหมดโดยเรียงจากวันที่ น้อย ไป มาก
 * q = ออกโปรแกรม พร้อมบันทึก event หรือ task
 */
	public void test() throws Exception {
		cm.loadData();
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println(cm.getWelcomeMsg());
		while (true) {
			System.out
					.print("Chooes action (c = create, d = display, q = quit) : ");
			String action = br.readLine().trim();

			if (action.startsWith("c")) {
				System.out.print("Create task or event (t, e) : ");
				String type = br.readLine().trim();
				String data;
				String[] info;
				
				if (type.equals("e")) {
					System.out.print("Add event (desc, from, to) : ");

				} else {
					System.out.print("Add task (desc, due date, est. time duration) : ");
				}					
				data = br.readLine().trim();
				info = data.split(",");
				cm.create(type,info[0], info[1], info[2]);
				System.out.println(cm.getDoneMsg());
			} else if (action.startsWith("d")) {
				System.out.println(cm.display(cm.getTimeDurByDate(), cm.getTotalTime()));
//				System.out.println("Items\t\t\tType\t\tEst.Time\tDate\t\tDone");
//				System.out.println("=====\t\t\t====\t\t========\t====\t\t====");
////				Collections.sort(ac.list);
//				for (int i = 0; i < ac.list.size(); i++) {
//					String type = ac.list.get(i).getClass().getSimpleName();
//					String done;
//					if (type.endsWith("Event")) {
//						done = "--";
//					} else {
//						done = "NO";
//					}
//					System.out.println(ac.list.get(i).getDescription() + "\t\t"
//							+ type + "\t\t" + ac.list.get(i).getDuration()
//							+ "\t\t"
//							+ dateFormat.format(ac.list.get(i).getDate())
//							+ "\t" + done);
//				}
//				System.out.println("\nTotal estimated time: "
//						+ ac.getTotalEstimatedTime() + " minutes");
			} else if (action.startsWith("q")) {
				cm.saveData();
			} else {
				System.out.println("NOOOOOOOOO");
			}
		}
	}
}
