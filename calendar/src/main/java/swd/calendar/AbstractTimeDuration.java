package swd.calendar;

public abstract class AbstractTimeDuration implements TimeDuration {
	protected String description;
	
	public void setDescriotion(String desc){
		description = desc;
	}
	
	public String getDescription(){
		return description;
	}
	
	public int compareTo(TimeDuration o){
		return this.getDate().compareTo(o.getDate());
	}
}
