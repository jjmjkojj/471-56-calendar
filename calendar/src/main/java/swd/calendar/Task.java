package swd.calendar;

import java.util.Date;

public class Task extends AbstractTimeDuration {
//	private String description;
	private Date dueDate;
	private Boolean done ;
	private int estTime;
/**
 * 
 * @param desc รับชื่องาน
 * @param date รับวันที่เริ่มงาน
 * @param estTime รับเวลาที่ใช้ในการทำงาน
 */
	public Task(String desc,Date date,int estTime){
		setDescriotion(desc);
		this.dueDate = date;
		this.estTime = estTime;
	}
	/**
	 * 
	 * @return สถานะงาน ว่่าทำเสร็จหรือยังไม่เสร็จ
	 */
	public Boolean isDone(){
		return done;
	}
	/**
	 * เปลี่ยนสถานะงาน ให้เป็น เสร็จแล้ว
	 */
	public void done(){
		done = true;
	}
	/**
	 * เปลี่ยนสถานะงานให้เป็น ยังไม่เสร็จ
	 */
	public void notDone(){
		done = false;
	}
	/**
	 * return วันที่เริ่มงาน
	 */
	public Date getDate(){
		return dueDate;
	}
	/**
	 * return ชืื่องาน
	 */
//	public String getDescription(){
//		return getDescription();
//	}
	/**
	 * return เวลาที่ใช้ในการทำงาน
	 */
	public int getDuration(){
		return estTime;
	}

//	public int compareTo(TimeDuration o) {
//		// TODO Auto-generated method stub
//		return this.getDate().compareTo(o.getDate());
//	}
	
}
