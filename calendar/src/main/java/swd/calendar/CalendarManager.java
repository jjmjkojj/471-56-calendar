package swd.calendar;

import java.awt.List;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Observable;

public class CalendarManager  extends Observable{
	Account ac;
	FileManage fm;
	Event event;
	Task task;
	CalendarView cv;
	private DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
	private ArrayList<TimeDuration> list;
	private String tmp = "";
	
	public CalendarManager(Account ac2) {
		ac = ac2;
		cv = new CalendarView();
		list = ac.list;
	}

	public String create(String type, String desc, String data1, String data2) throws ParseException{
		if (type.equals("Event")) {
			event = new Event(desc, dateTimeFormat.parse(data1), dateTimeFormat.parse(data2));
			ac.add(event);
		}else {
			task = new Task(desc, dateFormat.parse(data1), Integer.parseInt(data2));
			ac.add(task);
		}
		return "";
	}
	
	public String display(ArrayList<TimeDuration> t, int total) {
		tmp = cv.getResult(t, total);
		return tmp;
	}
	
	public ArrayList<TimeDuration> getTimeDurByDate(){
//		System.out.println(list);
		Collections.sort(list,new DateComparator());
//		System.out.println(ac.list+" <----");
		return list;
	}
	
	public ArrayList<TimeDuration> getTimeDurByDesc(){
		Collections.sort(list, new DescriptionComparator());
		return list;
	}
	
	public ArrayList<TimeDuration> getTimeDurByDuration(){
		Collections.sort(list, new DurationComparator());
		return list;
	}
	
	public int getTotalTime(){
		return ac.getTotalEstimatedTime();
	}
	
	public String getWelcomeMsg(){
		return cv.getWelcomeMsg();
	}
	
	public String getDoneMsg(){
		return cv.getDoneMsg();
	}
	
	public void loadData() throws Exception{
		fm = new FileManage(ac);
		fm.readFile();
	}
	
	public void saveData() throws Exception{
		fm = new FileManage(ac);
		fm.writeFile();
	}
	
	public class DateComparator implements Comparator<TimeDuration>{

		public int compare(TimeDuration o1, TimeDuration o2) {
//			// TODO Auto-generated method stub
			if (o1.getDate().getTime() < o2.getDate().getTime())
				return -1;
			if (o1.getDate().getTime() > o2.getDate().getTime())
				return 1;
			return 0;
		}
		
	}
	
	public class DescriptionComparator implements Comparator<TimeDuration>{

		public int compare(TimeDuration o1, TimeDuration o2) {
			// TODO Auto-generated method stub
			return o1.getDescription().compareToIgnoreCase(o2.getDescription());
		}
		
	}
	
	public class DurationComparator implements Comparator<TimeDuration>{

		public int compare(TimeDuration o1, TimeDuration o2) {
			// TODO Auto-generated method stub
			return o1.getDuration()-o2.getDuration();
		}
		
	}

	
}

