package swd.calendar;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class EventTest {

	private Event event;
	private DateFormat dateTimeFormat;
	private Date dateFrom,dateTo;
	
	@Before
	public void setup() throws ParseException{
		dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
		dateFrom = dateTimeFormat.parse("01/07/13 14:00");
		dateTo = dateTimeFormat.parse("2/07/13 14:00");
	}
	
	@Test
	public void testGetEsimatedTime() throws ParseException {
		event = new Event("itme for test", dateFrom, dateTo);
		assertEquals(1440, event.getDuration());
	}

}
