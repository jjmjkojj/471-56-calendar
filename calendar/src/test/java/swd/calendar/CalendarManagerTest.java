package swd.calendar;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

public class CalendarManagerTest {
	private Account ac;
	private CalendarManager cm;
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
	private ArrayList<TimeDuration> list;
	

	@Before
	public void setup() throws ParseException{
		ac = new Account();
		cm = new CalendarManager(ac);
		cm.create("Task", "1", "5/5/2013", "100");
		cm.create("Task", "2", "4/4/2013", "90");
		cm.create("Task", "3", "3/3/2013", "70");
		cm.create("Task", "4", "2/2/2013", "80");
		cm.create("Task", "5", "1/1/2013", "110");
	}
	@Test
	public void testGetTimeDurByDate() {
		list = cm.getTimeDurByDate();
		assertEquals("5", list.get(0).getDescription());
		assertEquals("4", list.get(1).getDescription());
		assertEquals("3", list.get(2).getDescription());
		assertEquals("2", list.get(3).getDescription());
		assertEquals("1", list.get(4).getDescription());
	}
	@Test
	public void testGetTimeDurByDesc(){
		list = cm.getTimeDurByDesc();
		assertEquals("1", list.get(0).getDescription());
		assertEquals("2", list.get(1).getDescription());
		assertEquals("3", list.get(2).getDescription());
		assertEquals("4", list.get(3).getDescription());
		assertEquals("5", list.get(4).getDescription());
	}
	@Test
	public void testGetTimeDurByDuration(){
		list = cm.getTimeDurByDuration();
		assertEquals("3", list.get(0).getDescription());
		assertEquals("4", list.get(1).getDescription());
		assertEquals("2", list.get(2).getDescription());
		assertEquals("1", list.get(3).getDescription());
		assertEquals("5", list.get(4).getDescription());
	}
	
	

}
