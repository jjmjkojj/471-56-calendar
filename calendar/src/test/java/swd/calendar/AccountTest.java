package swd.calendar;

import static org.junit.Assert.*;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import org.junit.Before;
import org.junit.Test;

public class AccountTest {
	private DateFormat dateTimeFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
	private DateFormat dateFormat = new SimpleDateFormat("dd/MM/yy");
	private Account ac = new Account();
	
	@Before
	public void setup() throws ParseException{
		ac.listForTest.add(0,new Task("0", dateFormat.parse("31/12/2012"), 50));
		ac.listForTest.add(1,new Event("1", dateTimeFormat.parse("1/1/2013 1:00"), dateTimeFormat.parse("1/1/2013 2:00")));
		ac.listForTest.add(2,new Task("2",dateFormat.parse("2/1/2013"),22));
		ac.listForTest.add(3,new Task("3",dateFormat.parse("3/1/2013"),33));
		ac.listForTest.add(4,new Event("4",dateTimeFormat.parse("4/1/2013 4:00"),dateTimeFormat.parse("5/1/2013 5:00")));
		ac.add(new Task("3",dateFormat.parse("3/1/2013"),33));
		
		ac.add(new Event("1", dateTimeFormat.parse("1/1/2013 1:00"), dateTimeFormat.parse("1/1/2013 2:00")));
		
		ac.add(new Task("2",dateFormat.parse("2/1/2013"),22));
		
		ac.add(new Event("4",dateTimeFormat.parse("4/1/2013 4:00"),dateTimeFormat.parse("5/1/2013 5:00")));
		
		ac.add(new Task("0", dateFormat.parse("31/12/2012"), 50));
		
	}
	
	@Test
	public void testIndex0() {
		assertEquals(ac.listForTest.get(0).getDescription(), ac.testSort(0));
	}
	@Test
	public void testIndex1() {
		assertEquals(ac.listForTest.get(1).getDescription(), ac.testSort(1));
	}
	@Test
	public void testIndex2() {
		assertEquals(ac.listForTest.get(2).getDescription(), ac.testSort(2));
	}
	@Test
	public void testIndex3() {
		assertEquals(ac.listForTest.get(3).getDescription(), ac.testSort(3));
	}
	@Test
	public void testIndex4() {
		assertEquals(ac.listForTest.get(4).getDescription(), ac.testSort(4));
	}

}
